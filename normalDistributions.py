#import libraries
import numpy as np
import matplotlib.pyplot as plt


#Define a function for generating a Gaussian distribution with a given mean and standard deviation
def makeGaussFunc(x,mean,sigma):
    gaussFunc=(1/(sigma*np.sqrt(2*np.pi)))*np.exp((-((x-mean)**2)/2)/((sigma**2)))
    return gaussFunc

#Generate three different gaussian distributions using the above function:
x=np.linspace(-20,20,512)
gaussFunc_10_2=makeGaussFunc(x,10,2)   #mean=10, sigma=2
gaussFunc_neg5_4=makeGaussFunc(x,-5,4)     #mean=-5, sigma=4 
gaussFunc_0_1=makeGaussFunc(x,0,1)     #mean=0, sigma=1

#Now plot the distributions:
plt.plot(x,gaussFunc_10_2,label=r'$eN(10,2^2)$')
plt.plot(x,gaussFunc_neg5_4,label=r'$N(-5,4^2)$')
plt.plot(x,gaussFunc_0_1,label=r'$N(0,1^2)$')
plt.ylim(0,0.42)
plt.xlabel('X',fontsize=16)
plt.ylabel('P(X)',fontsize=16)
plt.title('Normal Distributions',fontsize=20)
plt.legend(loc='upper right')
plt.show()