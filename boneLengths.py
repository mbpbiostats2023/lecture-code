#import libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

#Find the z-score for a bone length of 66 mm:
mean = 60
stdev = 10
z66 = (66 - mean) / stdev

#find the probability of a bone length greater than 66 mm
p66 = 1 - norm.cdf(z66)
print("The probability of a bone length greater than 66 mm is",p66)

