#import libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

#Find the CDF for a z-value of -3.01
print("The probability of a z-score <= -3.01 is",norm.cdf(-3.01))

#find the CDF for a z-value of -2.28
print("The probability of a z-score <= -2.28 is",norm.cdf(-2.28))
