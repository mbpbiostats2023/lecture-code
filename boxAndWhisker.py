#import libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
import seaborn as sns
from statsmodels.distributions.empirical_distribution import ECDF 


#Start by making a synthetic sample drawing random values from a normal distribution:
mean = 33
stdev = 7
N = 133
sample = np.random.normal(mean,stdev,N)

#Now make a box and whisker plot of the data:
plt.boxplot(sample)
plt.xlabel("Group",fontsize=16)
plt.ylabel("Age",fontsize=16)
plt.title("Box and Whisker Plot",fontsize=20)
plt.show()