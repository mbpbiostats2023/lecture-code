#import libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
import seaborn as sns
from statsmodels.distributions.empirical_distribution import ECDF 


#Start by making a synthetic sample drawing random values from a normal distribution:
mean = 33
stdev = 7
N = 133
sample = np.random.normal(mean,stdev,N)

#Now make a beeswarm plot of the sample:
sns.swarmplot(data=sample)
plt.xlabel("Group",fontsize=16)
plt.ylabel("Age",fontsize=16)
plt.title("Beeswarm plot",fontsize=20)
plt.show()

#Now make the empirical cumultaive distribution function (ECDF):
CDF = ECDF(sample)
x=np.linspace(3,63,1000)
plt.plot(x,CDF(x))
plt.xlabel("Age",fontsize=16)
plt.ylabel("CDF",fontsize=16)
plt.title("Empirical Cumulative Distribution Function (ECDF)",fontsize=18)
plt.show()