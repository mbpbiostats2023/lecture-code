#import libraries
import numpy as np
import matplotlib.pyplot as plt

#Make a "continuous" probability density function:
x = np.linspace(0,2*np.pi,512)
dx = np.diff([x[0], x[1]])
PDF = np.sin(x/2)*3*np.exp(-2*x/2)

#Scale the PDF so that the total area under the curve is 1.0
area = np.sum(PDF) * dx
PDF = PDF / area

#Plot the PDF:
plt.plot(x,PDF)
plt.ylabel("P(X)",fontsize=16)
plt.xlabel("X",fontsize=16)
plt.title("PDF",fontsize=20)
plt.show()

#Calculate the cumulative distribution function:
CDF=np.cumsum(PDF) * dx

#Plot the cumulative distribution function:
plt.step(x,CDF)
plt.ylabel("CDF(X)",fontsize=16)
plt.xlabel("X",fontsize=16)
plt.title("Continuous CDF",fontsize=20)
plt.show()
