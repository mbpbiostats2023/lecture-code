#import libraries
import numpy as np
import matplotlib.pyplot as plt

#set up the probability mass function for a discrete random
#variable 'x' whose 
x = np.linspace(1,11,11)
freq = np.array([20,30,50,90,160,145,105,70,52,37,27])
PMF = freq / np.sum(freq)

#plot the probability mass function:
plt.stem(x,PMF,basefmt=" ")
plt.ylabel("P(X)",fontsize=16)
plt.xlabel("X",fontsize=16)
plt.ylim([0,0.22])
plt.title("PMF",fontsize=20)
plt.show()

#Calculate the cumulative distribution function:
CDF=np.cumsum(PMF)

#Plot the cumulative distribution function:
plt.step(x,CDF)
plt.ylabel("CDF(X)",fontsize=16)
plt.xlabel("X",fontsize=16)
plt.title("Discrete CDF",fontsize=20)
plt.show()