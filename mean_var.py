import numpy as np

#Define an array:
x = np.array([2.5, 4, 5, 6.66, 3, 3, 8.1])

#Calculate the Mean of x:
meanX = np.mean(x)

#Calculate the population variance of x:
varX_pop = np.var(x,ddof=0)

#Calculate the sample variance of x:
varX_samp = np.var(x,ddof=1)

#Calculate the population standard deviation of x two different ways:
stdX_pop_1 = np.sqrt(varX_pop)
stdX_pop_2 = np.std(x,ddof=0)

#Calculate the sample standard deviation of x two different ways:
stdX_samp_1 = np.sqrt(varX_samp)
stdX_samp_2 = np.std(x,ddof=1)

print("The sample/population mean is",meanX)
print("The population variance is",varX_pop)
print("The sample variance is",varX_samp)
print("The population standard deviation is",stdX_pop_1,"==",stdX_pop_2)
print("The sample standard deviation is",stdX_samp_1,"==",stdX_samp_2)
