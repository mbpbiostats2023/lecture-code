#import libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

#Start by making a synthetic sample drawing random values from a normal distribution:
mean = 33
stdev = 7
N = 133
sample = np.random.normal(mean,stdev,N)

#Now make a histogram with 12 bins
plt.hist(sample,bins=12)
plt.xlabel("Age",fontsize=16)
plt.ylabel("Frequency",fontsize=16)
plt.show()

#Now make a histogram of the same distribution, but with 35 bins
plt.hist(sample,bins=35)
plt.xlabel("Age",fontsize=16)
plt.ylabel("Frequency",fontsize=16)
plt.show()


